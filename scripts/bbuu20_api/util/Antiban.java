package scripts.bbuu20_api.util;

import org.tribot.api.General;
import org.tribot.api.util.abc.ABCUtil;

public class Antiban {

    /*
     Build static single instance object
    */
    private Antiban() {}
    private static Antiban antiban = new Antiban();
    public static Antiban get() {
        return antiban;
    }

    /*
     Instantiate abcutil class
    */
    private ABCUtil abcInstance = new ABCUtil();

    /*
     ***TIMED ACTIONS*** (25 pts)
     *
     * Checks timers based on our profile, performs timed action and prints action if bool returns true
     *
     * Our player is idling when we call this method
    */
    public void idleTimedActions() {
        if (abcInstance.shouldCheckTabs()) {
            System.out.println("Checking tabs.");
            General.sleep(General.randomSD(155, 4412, 982, 10)); //sleep after checking tabs
            abcInstance.checkTabs();
        }
        if (abcInstance.shouldCheckXP()) {
            System.out.println("Checking xp.");
            General.sleep(General.randomSD(155, 4412, 982, 10)); //sleep after checking xp
            abcInstance.checkXP();
        }
        if (abcInstance.shouldExamineEntity()) {
            System.out.println("Examining entity.");
            abcInstance.examineEntity();
        }
        if (abcInstance.shouldMoveMouse()) {
            System.out.println("Moving mouse.");
            abcInstance.moveMouse();
        }
        if (abcInstance.shouldPickupMouse()) {
            System.out.println("Picking up mouse.");
            General.sleep(General.randomSD(155, 2412, 382, 10)); //sleep after picking up mouse
            abcInstance.pickupMouse();
        }
        if (abcInstance.shouldRightClick()) {
            System.out.println("Right clicking.");
            abcInstance.rightClick();
        }
        if (abcInstance.shouldRotateCamera()) {
            System.out.println("Rotating camera.");
            abcInstance.rotateCamera();
        }
        if (abcInstance.shouldLeaveGame()) {
            System.out.println("Leaving game.");
            abcInstance.leaveGame();
            General.sleep(General.randomSD(83, 23141, 151, 1000)); //sleep after returning the mouse to the game
        }
    }
    /*
     ***Preferences*** (10 pts)
     *
     * Preferred ways of doing things based on our profile.
     *
     * Preferences already handled by default in this script:
     *      * Open Bank Preference handled by default
     *      * Tab Switching Preference handled by default
     *      * I had initially implemented getNextTarget, but decided it was useless in mining, and makes the bot look more bot-like.
     */
    public int getRunEnergyToActivate() {
        return abcInstance.generateRunActivation();
    }
    //public RSObject getNextTarget(Positionable[] targets) {
    //    return (RSObject) abcInstance.selectNextTarget(targets); //selects a target based on our profile
    //}
    /*
     ***Action Conditions*** (26 pts)
     *
     * Generates when something should be done based on our profile.
     *
     * Action Conditions already handled by this script:
     *      * HP to eat at not applicable
     *      * Energy to activate run at handled by Dax Walker
     *      * Moving to anticipated in not applicable to mining
     *      * Resource switching upon high competition is not applicable
     *      * Hovering over next target is not applicable
     */

    /*
     ***Reaction Times*** (40 pts)
     *
     * Generates reaction times using abc instead of using random sleeps
     */
    private int generateReactionTime(int waitingTime /*waiting time : amount of time spent mining the rock*/) {
        return abcInstance.generateReactionTime(abcInstance.generateBitFlags(waitingTime)); //return reaction time in ms
    } //Called right after finishing mining a rock
    public void generateSupportingTrackerInfo(int estimatedTime /*estimated time == average waiting time*/) { //Called right after clicking a rock
        abcInstance.generateTrackers(estimatedTime);
    }
    public void generateAndSleep(int waitingTime) { //sleeps for the generated amount of time. Called after reaction time is generated
        try {
            int time = (generateReactionTime(waitingTime) / 8); //the default reaction time is way too long, divide by 3 or more
            System.out.println("Sleeping for: " + time + " milliseconds.");
            abcInstance.sleep(time);
        } catch (InterruptedException e) {
            System.out.println("The background thread interrupted the abc sleep.");
        }
    }
}