package scripts.bbuu20_api.objhandlers;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.types.RSGroundItem;
import scripts.bbuu20_api.util.Antiban;

public class GroundItemHandler {
    public static boolean clickRSGroundItem(RSGroundItem[] groundItems) {
        long startWaitTime;
        boolean wasClicked = false;
        if (groundItems != null) {
            if (groundItems.length > 0) {
                startWaitTime = System.currentTimeMillis();
                groundItems[0].click();
                wasClicked = Timing.waitCondition(() ->  Timing.waitCrosshair(250) == 2, General.random(300, 800));
                Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
            }
        }
        return wasClicked;
    }
    public static boolean hoverRSGroundItem(RSGroundItem[] groundItems) {
        if (groundItems != null) {
            if (groundItems.length > 0) {
                return groundItems[0].hover();
            }
        }
        return false;
    }
}
