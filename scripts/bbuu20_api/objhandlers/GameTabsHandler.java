package scripts.bbuu20_api.objhandlers;

import org.tribot.api2007.GameTab;

public class GameTabsHandler {
    public static boolean openTab(GameTab.TABS gameTab) {
        if (gameTab != null) {
            if (!gameTab.isOpen()) {
                return gameTab.open();
            }
        }
        return false;
    }
}
