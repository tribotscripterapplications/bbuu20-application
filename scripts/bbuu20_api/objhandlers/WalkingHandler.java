package scripts.bbuu20_api.objhandlers;

import org.tribot.api.Timing;
import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSTile;
import scripts.bbuu20_api.util.Antiban;
import scripts.dax_api.api_lib.DaxWalker;

public class WalkingHandler {
    public static boolean walkAndSleep(RSTile tile) {
        long startWaitTime;
        boolean reachedDestination;
        DaxWalker.walkTo(tile);
        startWaitTime = System.currentTimeMillis();
        reachedDestination = Timing.waitCondition(() -> !Player.isMoving(), 10000);
        Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
        return reachedDestination;
    }
    public static boolean walkToBankAndSleep() {
        long startWaitTime;
        boolean reachedDestination;
        DaxWalker.walkToBank();
        startWaitTime = System.currentTimeMillis();
        reachedDestination = Timing.waitCondition(() -> !Player.isMoving(), 10000);
        Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
        return reachedDestination;
    }
}
