package scripts.bbuu20_api.objhandlers;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.types.RSItem;
import scripts.bbuu20_api.util.Antiban;

public class ItemHandler {
    public static boolean clickRSItem(RSItem[] items) {
        long startWaitTime;
        boolean wasClicked = false;
        if (items != null) {
            if (items.length > 0) {
                startWaitTime = System.currentTimeMillis();
                items[0].click();
                wasClicked = Timing.waitCondition(() ->  Timing.waitCrosshair(250) == 2, General.random(3000, 8000));
                Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
            }
        }
        return wasClicked;
    }
    public static boolean hoverRSItem(RSItem[] items) {
        if (items != null) {
            if (items.length > 0) {
                return items[0].hover();
            }
        }
        return false;
    }
}
