package scripts.bbuu20_api.objhandlers;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Camera;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.NPCChat;
import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSObject;
import scripts.bbuu20_api.util.Antiban;
import scripts.dax_api.api_lib.DaxWalker;

public class NPCHandler {
    static long startWaitTime;
    public static boolean clickRSNPC(RSNPC[] npcs) {
        return clickRSNPC(npcs, "");
    }
    public static boolean clickRSNPC(RSNPC[] npcs, String clickOption) {
        return clickRSNPC(npcs, clickOption, !Player.isMoving() && Player.getAnimation() == -1);
    }
    public static boolean clickRSNPC(RSNPC[] npcs, String clickOption, boolean waitUntil) {
        return clickRSNPC(npcs, clickOption, waitUntil, true);
    }
    public static boolean clickRSNPC(RSNPC[] npcs, String clickOption, boolean waitUntil, boolean walkToNPC) {
        return clickRSNPC(npcs, clickOption, waitUntil, walkToNPC, 15);
    }
    public static boolean clickRSNPC(RSNPC[] npcs, String clickOption, boolean waitUntil, boolean walkToNPC, int distanceToTurnAt) {
        boolean wasClicked = false;
        if (npcs != null) {
            if (npcs.length > 0) {
                if (npcs[0].isOnScreen()) {
                    if (npcs[0].isClickable()) {
                        startWaitTime = System.currentTimeMillis();
                        DynamicClicking.clickRSNPC(npcs[0], clickOption);
                        General.sleep(100);
                        wasClicked = Timing.waitCondition(() -> waitUntil, General.random(3000, 6000));
                        Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
                    }
                }
                else {
                    if (walkToNPC) {
                        startWaitTime = System.currentTimeMillis();
                        DaxWalker.walkTo(npcs[0]);
                        Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
                    }
                    else if (Player.getPosition().distanceTo(npcs[0]) < distanceToTurnAt) {
                        Camera.turnToTile(npcs[0]);
                    }
                }
            }
        }
        return wasClicked;
    }
    public static boolean hoverRSNpc(RSObject[] npcs) {
        if (npcs != null) {
            if (npcs.length > 0) {
                return npcs[0].hover();
            }
        }
        return false;
    }
    public static boolean continueChatAndSleep() {
        return continueChatAndSleep("");
    }
    public static boolean continueChatAndSleep(String optionToSelect) {
        boolean wasContinued;
        if (InterfaceHandler.isOnScreen(Interfaces.get(219, 0, 1))) {
            selectChatOptionAndSleep(optionToSelect);
        }
        startWaitTime = System.currentTimeMillis();
        wasContinued = NPCChat.clickContinue(true);
        Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
        return wasContinued;
    }
    public static boolean selectChatOptionAndSleep(String optionToSelect) {
        boolean wasClicked = false;
        for (int currentChatOptions = 1;
                 InterfaceHandler.isOnScreen(Interfaces.get(219, 1, currentChatOptions));
                 currentChatOptions++) {
            if (Interfaces.get(219, 1, currentChatOptions).getText().contains(optionToSelect)) {
                startWaitTime = System.currentTimeMillis();
                wasClicked = InterfaceHandler.clickInterface(Interfaces.get(219, 1, currentChatOptions));
                Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
            }
        }
        return wasClicked;
    }
}
