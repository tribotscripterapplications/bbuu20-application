package scripts.bbuu20_api.objhandlers;

import org.tribot.api.Timing;
import org.tribot.api2007.types.RSInterface;
import scripts.bbuu20_api.util.Antiban;

public class InterfaceHandler {

    public static boolean isOnScreen(RSInterface rsInterface) {
        if (rsInterface != null) {
            return !rsInterface.isHidden();
        }
        return false;
    }
    public static boolean clickInterface(RSInterface rsInterface) {
        return clickInterface(rsInterface, "");
    }
    public static boolean clickInterface(RSInterface rsInterface, String getText) {
        long startWaitTime;
        boolean isOnScreen = isOnScreen(rsInterface);
        if (isOnScreen) {
            if (rsInterface.getText().contains(getText)) {
                rsInterface.click();
                startWaitTime = System.currentTimeMillis();
                Timing.waitCondition(() -> Timing.waitCrosshair(1500) == 2 || rsInterface.isHidden(), 2000);
                Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
            }
        }
        return false;
    }
}
