package scripts.bbuu20_api.objhandlers;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Camera;
import org.tribot.api2007.Player;
import org.tribot.api2007.Walking;
import org.tribot.api2007.types.RSObject;
import scripts.bbuu20_api.util.Antiban;

public class ObjectHandler {

    public static boolean clickRSObject(RSObject[] objects) {
        return clickRSObject(objects, "");
    }
    public static boolean clickRSObject(RSObject[] objects, String clickOption) {
        return clickRSObject(objects, clickOption, !Player.isMoving() && Player.getAnimation() == -1);
    }
    public static boolean clickRSObject(RSObject[] objects, String clickOption, boolean waitUntil) {
        return clickRSObject(objects, clickOption, waitUntil, true);
    }
    public static boolean clickRSObject(RSObject[] objects, String clickOption, boolean waitUntil, boolean walkToObject) {
        return clickRSObject(objects, clickOption, waitUntil, walkToObject, 5);
    }
    public static boolean clickRSObject(RSObject[] objects, String clickOption, boolean waitUntil, boolean walkToObject, int distanceToTurnCamera) {
        long startWaitTime;
        boolean wasClicked = false;
        if (objects != null) {
            if (objects.length > 0) {
                if (objects[0].isOnScreen()) {
                    if (objects[0].isClickable()) {
                        startWaitTime = System.currentTimeMillis();
                        DynamicClicking.clickRSObject(objects[0], clickOption);
                        General.sleep(100);
                        wasClicked = Timing.waitCondition(() -> waitUntil, General.random(3000, 8000));
                        Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
                    }
                }
                else {
                    if (Player.getPosition().distanceTo(objects[0]) < distanceToTurnCamera) {
                        Camera.turnToTile(objects[0]);
                    }
                    else if (walkToObject) {
                        startWaitTime = System.currentTimeMillis();
                        Walking.walkTo(objects[0]);
                        Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
                    }
                }
            }
        }
        return wasClicked;
    }
    public static boolean hoverRSObject(RSObject[] objects) {
        if (objects != null) {
            if (objects.length > 0) {
                return objects[0].hover();
            }
        }
        return false;
    }
}