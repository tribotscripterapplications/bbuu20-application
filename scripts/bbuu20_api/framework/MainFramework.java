package scripts.bbuu20_api.framework;

public abstract class MainFramework {

    public abstract String status();

    public abstract boolean shouldProceed();

    public abstract void proceed();
}