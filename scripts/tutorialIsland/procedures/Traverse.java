package scripts.tutorialIsland.procedures;

import org.tribot.api2007.Player;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.bbuu20_api.objhandlers.WalkingHandler;
import scripts.tutorialIsland.data.Finals;
import scripts.tutorialIsland.data.SubTask;
import scripts.tutorialIsland.data.Vars;

public class Traverse extends MainFramework {
    @Override
    public String status() {
        return "Traversing";
    }

    @Override
    public boolean shouldProceed() {
        return  Vars.get().currentSubTask == SubTask.WALKING_TO_SURVIVAL_AREA
                || Vars.get().currentSubTask == SubTask.WALKING_TO_KITCHEN
                || Vars.get().currentSubTask == SubTask.WALKING_TO_QUEST_GUIDE
                || Vars.get().currentSubTask == SubTask.WALKING_TO_MINING_AREA
                || Vars.get().currentSubTask == SubTask.WALKING_TO_COMBAT_AREA
                || Vars.get().currentSubTask == SubTask.WALKING_TO_BANK
                || Vars.get().currentSubTask == SubTask.WALKING_TO_ADVISER
                || (Vars.get().currentSubTask == SubTask.WALKING_AND_TALKING_TO_PRIEST
                && !Finals.PRIEST_GUIDE_HOUSE.contains(Player.getPosition()))
                || Vars.get().currentSubTask == SubTask.WALKING_TO_WIZARD;
    }

    @Override
    public void proceed() {
        switch (Vars.get().currentSubTask) {
            case WALKING_TO_SURVIVAL_AREA:
                WalkingHandler.walkAndSleep(Finals.SURVIVAL_AREA.getRandomTile());
                break;
            case WALKING_TO_KITCHEN:
                WalkingHandler.walkAndSleep(Finals.KITCHEN_AREA.getRandomTile());
                break;
            case WALKING_TO_QUEST_GUIDE:
                WalkingHandler.walkAndSleep(Finals.QUEST_GUIDE_HOUSE.getRandomTile());
                break;
            case WALKING_TO_MINING_AREA:
                WalkingHandler.walkAndSleep(Finals.MINING_AREA.getRandomTile());
                //ladders = Objects.findNearest(20, 9726);
                //ObjectHandler.clickRSObject(ladders);
                break;
            case WALKING_TO_COMBAT_AREA:
                WalkingHandler.walkAndSleep(Finals.COMBAT_AREA.getRandomTile());
                break;
            case WALKING_TO_BANK:
                WalkingHandler.walkAndSleep(Finals.BANK_AREA.getRandomTile());
                break;
            case WALKING_TO_ADVISER:
                WalkingHandler.walkAndSleep(Finals.ADVISER_GUIDE_ROOM.getRandomTile());
                break;
            case WALKING_AND_TALKING_TO_PRIEST:
                if (!Finals.PRIEST_GUIDE_HOUSE.contains(Player.getPosition()))
                    WalkingHandler.walkAndSleep(Finals.PRIEST_GUIDE_HOUSE.getRandomTile());
                break;
            case WALKING_TO_WIZARD:
                WalkingHandler.walkAndSleep(Finals.WIZARD_HOUSE.getRandomTile());
                break;
        }
    }
}
