package scripts.tutorialIsland.procedures.survivalarea;

import org.tribot.api2007.NPCs;
import org.tribot.api2007.types.RSNPC;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.bbuu20_api.objhandlers.NPCHandler;
import scripts.tutorialIsland.data.SubTask;
import scripts.tutorialIsland.data.Vars;

public class TalkToSurvivalExpert extends MainFramework {
    @Override
    public String status() {
        return "Talking to Survival Expert";
    }

    @Override
    public boolean shouldProceed() {
        return Vars.get().currentSubTask == SubTask.TALKING_TO_SURVIVAL_EXPERT;
    }

    @Override
    public void proceed() {
        RSNPC[] survivalExpert = NPCs.findNearest("Survival Expert");
        if (!NPCHandler.continueChatAndSleep()) {
            NPCHandler.clickRSNPC(survivalExpert);
        }
    }
}
