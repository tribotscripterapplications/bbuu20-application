package scripts.tutorialIsland.procedures.survivalarea;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.Mouse;
import org.tribot.api2007.*;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSTile;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.bbuu20_api.objhandlers.InterfaceHandler;
import scripts.bbuu20_api.objhandlers.NPCHandler;
import scripts.bbuu20_api.util.Antiban;
import scripts.tutorialIsland.data.SubTask;
import scripts.tutorialIsland.data.Vars;

public class HandleSurvivalArea extends MainFramework {
    private String statusUpdate = "Survival Area";

    @Override
    public String status() {
        return statusUpdate;
    }

    @Override
    public boolean shouldProceed() {
        return Vars.get().currentSubTask == SubTask.OPENING_INVENTORY
                || Vars.get().currentSubTask == SubTask.FISHING
                || Vars.get().currentSubTask == SubTask.OPENING_STATS_TAB
                || Vars.get().currentSubTask == SubTask.CHOPPING_TREE
                || Vars.get().currentSubTask == SubTask.MAKING_FIRE
                || Vars.get().currentSubTask == SubTask.COOKING_SHRIMPS;
    }

    @Override
    public void proceed() {
        RSObject[] fires;
        long startWaitTime;
        switch (Vars.get().currentSubTask) {
            case OPENING_INVENTORY:
                statusUpdate = "Opening Inventory";
                if (NPCChat.getMessage() != null) {
                    NPCHandler.continueChatAndSleep();
                }
                else if (InterfaceHandler.clickInterface(Interfaces.get(193, 0, 2))) {
                    break;
                }
                else {
                    GameTab.TABS.INVENTORY.open();
                    startWaitTime = System.currentTimeMillis();
                    Timing.waitCondition(GameTab.TABS.INVENTORY::isOpen, 2000);
                    Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
                }
                break;
            case FISHING:
                statusUpdate = "Fishing";
                RSNPC[] fishingSpot = NPCs.findNearest("Fishing spot");
                NPCHandler.clickRSNPC(fishingSpot);
                break;
            case OPENING_STATS_TAB:
                statusUpdate = "Opening Stats";
                GameTab.TABS.STATS.open();
                startWaitTime = System.currentTimeMillis();
                Timing.waitCondition(GameTab.TABS.STATS::isOpen, 2000);
                Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
                break;
            case CHOPPING_TREE:
                statusUpdate = "Chopping Tree";
                RSObject[] trees = Objects.findNearest(8, "Tree");
                int randomTree = General.random(0, 4);
                if (trees != null) {
                    if (randomTree <= trees.length) {
                        if (trees[randomTree].isClickable() && trees[randomTree].isOnScreen()) {
                            startWaitTime = System.currentTimeMillis();
                            trees[randomTree].hover();
                            Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep

                            startWaitTime = System.currentTimeMillis();
                            DynamicClicking.clickRSObject(trees[randomTree], "");
                            Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep

                            startWaitTime = System.currentTimeMillis();
                            Timing.waitCondition(() -> Inventory.getCount("Logs") > 0, 5000);
                            Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
                        }
                    }
                }
                break;
            case MAKING_FIRE:
                RSItem[] tinderboxes = Inventory.find("Tinderbox");
                RSItem[] logItem = Inventory.find("Logs");
                fires = Objects.findNearest(1, 26185);
                RSTile playerPosition;
                for (RSObject fire : fires) {
                    if (fire.getPosition().equals(Player.getPosition())) {
                        Walking.walkTo(new RSTile(Player.getPosition().getX(), Player.getPosition().getY() - 1, Player.getPosition().getPlane()));
                    }
                }
                startWaitTime = System.currentTimeMillis();
                tinderboxes[0].hover();
                Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep

                startWaitTime = System.currentTimeMillis();
                tinderboxes[0].click();
                Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep

                startWaitTime = System.currentTimeMillis();
                logItem[0].hover();
                Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep

                startWaitTime = System.currentTimeMillis();
                playerPosition = Player.getPosition();
                logItem[0].click();
                Timing.waitCondition(() -> !Player.getPosition().equals(playerPosition), 5000);
                Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
                break;
            case COOKING_SHRIMPS:
                RSItem[] shrimps = Inventory.find("Raw shrimps");
                fires = Objects.findNearest(1, 26185);

                if (fires != null) {
                    startWaitTime = System.currentTimeMillis();
                    shrimps[0].hover();
                    Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep

                    startWaitTime = System.currentTimeMillis();
                    shrimps[0].click();
                    Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep

                    startWaitTime = System.currentTimeMillis();
                    fires[0].hover();
                    Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep

                    startWaitTime = System.currentTimeMillis();
                    Mouse.click(3);
                    Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep

                    startWaitTime = System.currentTimeMillis();
                    ChooseOption.select("Use Raw shrimps -> Fire");
                    Timing.waitCondition(() -> Inventory.getCount("Shrimps") > 0, 5000);
                    Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
                }
                break;
        }
    }
}
