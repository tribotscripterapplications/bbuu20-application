package scripts.tutorialIsland.procedures.questhouse;

import org.tribot.api.Timing;
import org.tribot.api2007.*;
import org.tribot.api2007.types.RSNPC;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.bbuu20_api.objhandlers.InterfaceHandler;
import scripts.bbuu20_api.objhandlers.NPCHandler;
import scripts.bbuu20_api.util.Antiban;
import scripts.tutorialIsland.data.SubTask;
import scripts.tutorialIsland.data.Vars;

public class TalkToQuestGuide extends MainFramework {
    @Override
    public String status() {
        return "Talking to Quest Guide";
    }

    @Override
    public boolean shouldProceed() {
        return Vars.get().currentSubTask == SubTask.TALKING_TO_QUEST_GUIDE
                || Vars.get().currentSubTask == SubTask.OPENING_QUESTS_TAB;
    }

    @Override
    public void proceed() {
        long startWaitTime;
        switch (Vars.get().currentSubTask) {
            case TALKING_TO_QUEST_GUIDE:
                RSNPC[] questGuide = NPCs.find("Quest Guide");
                InterfaceHandler.clickInterface((Interfaces.get(193, 0, 2)));
                if (!NPCHandler.continueChatAndSleep())
                    NPCHandler.clickRSNPC(questGuide);
                break;
            case OPENING_QUESTS_TAB:
                GameTab.TABS.QUESTS.open();
                startWaitTime = System.currentTimeMillis();
                Timing.waitCondition(GameTab.TABS.QUESTS::isOpen, 3500);
                Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
                break;
        }
    }
}