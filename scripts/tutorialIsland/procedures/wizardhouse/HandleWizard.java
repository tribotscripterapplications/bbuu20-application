package scripts.tutorialIsland.procedures.wizardhouse;

import org.tribot.api2007.*;
import org.tribot.api2007.types.RSNPC;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.bbuu20_api.objhandlers.InterfaceHandler;
import scripts.bbuu20_api.objhandlers.NPCHandler;
import scripts.tutorialIsland.data.SubTask;
import scripts.tutorialIsland.data.Vars;

public class HandleWizard extends MainFramework {
    @Override
    public String status() {
        return "Wizard House";
    }

    @Override
    public boolean shouldProceed() {
        return (Vars.get().currentSubTask == SubTask.TALKING_TO_WIZARD
                || Vars.get().currentSubTask == SubTask.OPENING_MAGIC_TAB
                || Vars.get().currentSubTask == SubTask.SELECTING_SPELL
                || Vars.get().currentSubTask == SubTask.CLOSING_TIP
                || Vars.get().currentSubTask == SubTask.SELECTING_WIZARD_OPTION_ONE
                || Vars.get().currentSubTask == SubTask.SELECTING_WIZARD_OPTION_THREE);
    }

    @Override
    public void proceed() {
        RSNPC[] wizard = NPCs.find("Magic Instructor");
        RSNPC[] chicken = NPCs.find("Chicken");

        switch (Vars.get().currentSubTask) {
            case TALKING_TO_WIZARD:
                if (!NPCHandler.continueChatAndSleep()) {
                    NPCHandler.clickRSNPC(wizard);
                }
                break;
            case OPENING_MAGIC_TAB:
                GameTab.TABS.MAGIC.open();
                break;
            case SELECTING_SPELL:
                Magic.selectSpell("Wind strike");
                NPCHandler.clickRSNPC(chicken);
                break;
            case CLOSING_TIP:
                InterfaceHandler.clickInterface(Interfaces.get(193, 0, 2));
                break;
            case SELECTING_WIZARD_OPTION_ONE:
                InterfaceHandler.clickInterface(Interfaces.get(219, 1, 1));
                break;
            case SELECTING_WIZARD_OPTION_THREE:
                InterfaceHandler.clickInterface(Interfaces.get(219, 1, 3));
                break;
        }
    }
}
