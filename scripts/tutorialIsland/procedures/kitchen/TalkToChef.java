package scripts.tutorialIsland.procedures.kitchen;

import org.tribot.api2007.NPCChat;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.types.RSNPC;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.bbuu20_api.objhandlers.NPCHandler;
import scripts.tutorialIsland.data.SubTask;
import scripts.tutorialIsland.data.Vars;

public class TalkToChef extends MainFramework {
    @Override
    public String status() {
        return "Talking to Chef";
    }

    @Override
    public boolean shouldProceed() {
        return Vars.get().currentSubTask == SubTask.TALKING_TO_CHEF;
    }

    @Override
    public void proceed() {
        RSNPC[] cookingExpert = NPCs.findNearest("Master Chef");
        if (NPCChat.getMessage() != null) {
            NPCHandler.continueChatAndSleep();
            return;
        }
        NPCHandler.clickRSNPC(cookingExpert);
    }
}