package scripts.tutorialIsland.procedures.kitchen;

import org.tribot.api.Timing;
import org.tribot.api2007.*;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSObject;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.bbuu20_api.objhandlers.InterfaceHandler;
import scripts.bbuu20_api.objhandlers.ObjectHandler;
import scripts.bbuu20_api.util.Antiban;
import scripts.tutorialIsland.data.SubTask;
import scripts.tutorialIsland.data.Vars;

public class HandleKitchen extends MainFramework {
    private String statusUpdate = "Kitchen Area";
    @Override
    public String status() {
        return statusUpdate;
    }

    @Override
    public boolean shouldProceed() {
        return Vars.get().currentSubTask == SubTask.MAKING_DOUGH
                || Vars.get().currentSubTask == SubTask.COOKING_BREAD;
    }

    @Override
    public void proceed() {
        long startWaitTime;
        switch (Vars.get().currentSubTask) {
            case MAKING_DOUGH:
                RSItem[] flourPot = Inventory.find("Pot of flour");
                RSItem[] waterBucket = Inventory.find("Bucket of water");

                startWaitTime = System.currentTimeMillis();
                flourPot[0].hover();
                Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep

                startWaitTime = System.currentTimeMillis();
                flourPot[0].click();
                Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep

                startWaitTime = System.currentTimeMillis();
                waterBucket[0].hover();
                Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep

                startWaitTime = System.currentTimeMillis();
                waterBucket[0].click();
                Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
                Timing.waitCondition(() -> Inventory.getCount("Bread dough") > 0, 1500);
                break;
            case COOKING_BREAD:
                RSObject[] range = Objects.findNearest(10, "Range");

                if (!InterfaceHandler.clickInterface(Interfaces.get(193, 0, 2))) {
                    ObjectHandler.clickRSObject(range);
                }
                break;
        }
    }
}
