package scripts.tutorialIsland.procedures.chapel;

import org.tribot.api2007.GameTab;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.types.RSNPC;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.bbuu20_api.objhandlers.NPCHandler;
import scripts.tutorialIsland.data.SubTask;
import scripts.tutorialIsland.data.Vars;

public class HandleChapel extends MainFramework {
    @Override
    public String status() {
        return "Chapel";
    }

    @Override
    public boolean shouldProceed() {
        return Vars.get().currentSubTask == SubTask.WALKING_AND_TALKING_TO_PRIEST
                || Vars.get().currentSubTask == SubTask.OPENING_PRAYER_TAB
                || Vars.get().currentSubTask == SubTask.OPENING_FRIENDS_TAB;
    }

    @Override
    public void proceed() {
        RSNPC[] brotherBrace = NPCs.find("Brother Brace");
        if (Vars.get().currentSubTask == SubTask.WALKING_AND_TALKING_TO_PRIEST) {
            if (!NPCHandler.continueChatAndSleep()) {
                NPCHandler.clickRSNPC(brotherBrace);
            }
        }
        if (Vars.get().currentSubTask == SubTask.OPENING_PRAYER_TAB) {
            if (!NPCHandler.continueChatAndSleep()) {
                GameTab.TABS.PRAYERS.open();
            }
        }
        if (Vars.get().currentSubTask == SubTask.OPENING_FRIENDS_TAB) {
            if (!NPCHandler.continueChatAndSleep()) {
                GameTab.TABS.FRIENDS.open();
            }
        }
    }
}
