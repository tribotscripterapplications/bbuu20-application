package scripts.tutorialIsland.procedures.combatarea;

import org.tribot.api2007.*;
import org.tribot.api2007.types.RSNPC;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.bbuu20_api.objhandlers.NPCHandler;
import scripts.tutorialIsland.data.SubTask;
import scripts.tutorialIsland.data.Vars;

public class TalkToCombatGuide extends MainFramework {
    @Override
    public String status() {
        return "Talking to Combat Guide";
    }

    @Override
    public boolean shouldProceed() {
        return Vars.get().currentSubTask == SubTask.TALKING_TO_COMBAT_GUIDE;
    }

    @Override
    public void proceed() {
        RSNPC[] combatGuide = NPCs.find("Combat Instructor");
        if (!NPCHandler.continueChatAndSleep()) {
            NPCHandler.clickRSNPC(combatGuide);
        }
    }
}
