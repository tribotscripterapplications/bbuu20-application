package scripts.tutorialIsland.procedures.combatarea;

import org.tribot.api.Timing;
import org.tribot.api2007.*;
import org.tribot.api2007.types.*;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.bbuu20_api.objhandlers.InterfaceHandler;
import scripts.bbuu20_api.objhandlers.NPCHandler;
import scripts.bbuu20_api.objhandlers.WalkingHandler;
import scripts.dax_api.api_lib.DaxWalker;
import scripts.tutorialIsland.data.Finals;
import scripts.tutorialIsland.data.SubTask;
import scripts.tutorialIsland.data.Vars;

import static scripts.tutorialIsland.data.Finals.RATPIT;

public class HandleCombatArea extends MainFramework {
    private String statusUpdate = "Mining Area";

    @Override
    public String status() {
        return statusUpdate;
    }

    @Override
    public boolean shouldProceed() {
        return (Vars.get().currentSubTask == SubTask.OPENING_EQUIPMENT_TAB
                || Vars.get().currentSubTask == SubTask.OPENING_ARMORY
                || Vars.get().currentSubTask == SubTask.EQUIPPING_DAGGER
                || Vars.get().currentSubTask == SubTask.CLOSING_ARMORY
                || Vars.get().currentSubTask == SubTask.EQUIPPING_WEAPONS
                || Vars.get().currentSubTask == SubTask.OPENING_COMBAT_TAB
                || Vars.get().currentSubTask == SubTask.ENTERING_RAT_PIT
                || Vars.get().currentSubTask == SubTask.CLICKING_RAT
                || Vars.get().currentSubTask == SubTask.IN_COMBAT
                || Vars.get().currentSubTask == SubTask.EQUIPPING_BOW_AND_ARROW
                || Vars.get().currentSubTask == SubTask.LEAVING_RAT_PIT);
    }

    @Override
    public void proceed() {
        RSItem[] weapon, secondary;
        RSNPC[] rats;
        switch (Vars.get().currentSubTask) {
            case OPENING_EQUIPMENT_TAB:
                if (NPCChat.getMessage() != null) {
                    NPCHandler.continueChatAndSleep();
                    break;
                }
                if (!GameTab.TABS.EQUIPMENT.isOpen()) {
                    GameTab.TABS.EQUIPMENT.open();
                    Timing.waitCondition(GameTab.TABS.EQUIPMENT::isOpen, 1500);
                }
                break;
            case OPENING_ARMORY:
                if (NPCChat.getMessage() != null) {
                    NPCHandler.continueChatAndSleep();
                    break;
                }
                if (GameTab.TABS.EQUIPMENT.isOpen()) {
                    InterfaceHandler.clickInterface(Interfaces.get(387, 1));
                }
                else {
                    GameTab.TABS.EQUIPMENT.open();
                    Timing.waitCondition(GameTab.TABS.EQUIPMENT::isOpen, 1500);
                }
                break;
            case EQUIPPING_DAGGER:
                if (Inventory.getCount("Bronze dagger") > 0) {
                    weapon = Inventory.find("Bronze dagger");
                    if (weapon != null) {
                        weapon[0].click();
                        Timing.waitCondition(() -> Inventory.getCount("Bronze dagger") == 0, 3000);
                    }
                }
                break;
            case CLOSING_ARMORY:
                InterfaceHandler.clickInterface(Interfaces.get(84, 4));
                break;
            case EQUIPPING_WEAPONS:
                weapon = Inventory.find("Bronze sword");
                secondary = Inventory.find("Wooden shield");
                if (weapon != null && secondary != null) {
                    weapon[0].click();
                    Timing.waitCondition(() -> Inventory.getCount("Bronze sword") == 0, 3000);
                    secondary[0].click();
                    Timing.waitCondition(() -> Inventory.getCount("Wooden shield") == 0, 3000);
                }
                break;
            case OPENING_COMBAT_TAB:
                if (!GameTab.TABS.COMBAT.isOpen()) {
                    GameTab.TABS.COMBAT.open();
                }
                break;
            case ENTERING_RAT_PIT:
                WalkingHandler.walkAndSleep(RATPIT.getRandomTile());
            case LEAVING_RAT_PIT:
                WalkingHandler.walkAndSleep(Finals.COMBAT_AREA.getRandomTile());
                break;
            case CLICKING_RAT:
                rats = NPCs.findNearest("Giant rat");
                NPCHandler.clickRSNPC(rats, "", Combat.isUnderAttack() && Combat.getTargetEntity() != null, false);
                break;
            case IN_COMBAT:
                rats = NPCs.findNearest("Giant rat");
                if (Combat.isUnderAttack()) {
                    Timing.waitCondition(() -> !Combat.isUnderAttack(), 15000);
                    return;
                }
                NPCHandler.clickRSNPC(rats, "", Combat.isUnderAttack() && Combat.getTargetEntity() != null, false);
                break;
            case EQUIPPING_BOW_AND_ARROW:
                weapon = Inventory.find("Shortbow");
                secondary = Inventory.find("Bronze arrow");
                if (weapon != null && secondary != null) {
                    weapon[0].click();
                    Timing.waitCondition(() -> Inventory.getCount("Shortbow") == 0, 3000);
                    secondary[0].click();
                    Timing.waitCondition(() -> Inventory.getCount("Bronze arrow") == 0, 3000);
                }
                break;
            case CLIMBING_LADDER:
                DaxWalker.walkTo(Finals.BANK_AREA.getRandomTile());
                break;
        }
    }
}
