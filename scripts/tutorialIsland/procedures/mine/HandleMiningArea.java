package scripts.tutorialIsland.procedures.mine;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.*;
import org.tribot.api2007.types.RSObject;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.bbuu20_api.objhandlers.InterfaceHandler;
import scripts.bbuu20_api.objhandlers.ObjectHandler;
import scripts.bbuu20_api.util.Antiban;
import scripts.dax_api.api_lib.DaxWalker;
import scripts.tutorialIsland.data.SubTask;
import scripts.tutorialIsland.data.Vars;

public class HandleMiningArea extends MainFramework {
    private String statusUpdate = "Mining Area";

    @Override
    public String status() {
        return statusUpdate;
    }

    @Override
    public boolean shouldProceed() {
        return Vars.get().currentSubTask == SubTask.MINING_TIN
                || Vars.get().currentSubTask == SubTask.MINING_COPPER
                || Vars.get().currentSubTask == SubTask.SMELTING_ORES
                || Vars.get().currentSubTask == SubTask.SMITHING_DAGGER;
    }

    @Override
    public void proceed() {
        int randomRock;
        RSObject[] rocks, furnace, anvil;
        long startWaitTime;
        switch (Vars.get().currentSubTask) {
            case MINING_TIN:
                rocks = Objects.findNearest(15, 10080);
                randomRock = General.random(0, 8);
                if (rocks != null) {
                    if (randomRock <= rocks.length) {
                        if (rocks[randomRock].isClickable() && rocks[randomRock].isOnScreen()) {
                            startWaitTime = System.currentTimeMillis();
                            rocks[randomRock].hover();
                            Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep

                            startWaitTime = System.currentTimeMillis();
                            DynamicClicking.clickRSObject(rocks[randomRock], "");
                            Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep

                            startWaitTime = System.currentTimeMillis();
                            Timing.waitCondition(() -> Inventory.getCount("Tin ore") > 0, 15000);
                            Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
                        }
                        DaxWalker.walkTo(rocks[randomRock]);
                        Timing.waitCondition(() -> !Player.isMoving(), 10000);
                    }
                }
                break;
            case MINING_COPPER:
                rocks = Objects.findNearest(20, 10079);
                randomRock = General.random(0, 8);
                if (rocks != null) {
                    if (rocks[randomRock] != null) {
                        if (rocks[randomRock].isClickable() && rocks[randomRock].isOnScreen()) {
                            startWaitTime = System.currentTimeMillis();
                            rocks[randomRock].hover();
                            Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep

                            startWaitTime = System.currentTimeMillis();
                            DynamicClicking.clickRSObject(rocks[randomRock], "");
                            Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep

                            startWaitTime = System.currentTimeMillis();
                            Timing.waitCondition(() -> Inventory.getCount("Copper ore") > 0, 15000);
                            Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
                        }
                        startWaitTime = System.currentTimeMillis();
                        DaxWalker.walkTo(rocks[randomRock]);
                        Timing.waitCondition(() -> !Player.isMoving(), 10000);
                        Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
                    }
                }
                break;
            case SMELTING_ORES:
                furnace = Objects.findNearest(15, "Furnace");
                ObjectHandler.clickRSObject(furnace);
                break;
            case SMITHING_DAGGER:
                anvil = Objects.findNearest(15, "Anvil");
                ObjectHandler.clickRSObject(anvil, "", Interfaces.get(312, 9) != null);
                InterfaceHandler.clickInterface(Interfaces.get(312, 9));
                break;
        }
    }
}
