package scripts.tutorialIsland.procedures.mine;

import org.tribot.api2007.NPCs;
import org.tribot.api2007.types.RSNPC;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.bbuu20_api.objhandlers.NPCHandler;
import scripts.tutorialIsland.data.SubTask;
import scripts.tutorialIsland.data.Vars;

public class TalkToMiningGuide extends MainFramework {
    @Override
    public String status() {
        return "Talking to Mining Guide";
    }

    @Override
    public boolean shouldProceed() {
        return Vars.get().currentSubTask == SubTask.TALKING_TO_MINING_GUIDE;
    }

    @Override
    public void proceed() {
        RSNPC[] miningGuide = NPCs.find("Mining Instructor");
        if (!NPCHandler.continueChatAndSleep()) {
            NPCHandler.clickRSNPC(miningGuide);
        }
    }
}
