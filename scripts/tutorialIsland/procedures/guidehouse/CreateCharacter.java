package scripts.tutorialIsland.procedures.guidehouse;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.Keyboard;
import org.tribot.api2007.Interfaces;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.bbuu20_api.objhandlers.InterfaceHandler;
import scripts.bbuu20_api.util.Antiban;
import scripts.tutorialIsland.data.SubTask;
import scripts.tutorialIsland.data.Vars;
import scripts.tutorialIsland.util.Calculations;

public class CreateCharacter extends MainFramework {

    @Override
    public String status() {
        return "Creating Character";
    }

    @Override
    public boolean shouldProceed() {
        return Vars.get().currentSubTask == SubTask.OPENING_TEXT
                || Vars.get().currentSubTask == SubTask.TYPING_NAME
                || Vars.get().currentSubTask == SubTask.CHOOSING_NAME
                || Vars.get().currentSubTask == SubTask.CONFIRMING_NAME
                || Vars.get().currentSubTask == SubTask.SELECTING_CHARACTER;
    }

    @Override
    public void proceed() {
        long startWaitTime;
        switch (Vars.get().currentSubTask) {
            case OPENING_TEXT:
                InterfaceHandler.clickInterface(Interfaces.get(558, 11));
                break;
            case TYPING_NAME:
                for (int index = 0; index < General.random(1, 11); index++) {
                    Keyboard.typeKeys(Calculations.generateRandomChar());
                    General.sleep(General.randomSD(41, 1312, 112, 5000)); //no need for abc sleep, just an attempt to emulate realistic typing
                }
                Keyboard.typeKeys('\n');
                startWaitTime = System.currentTimeMillis();
                Timing.waitCondition(() -> Interfaces.get(558, 11).getText() != null, 10000);
                Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
                break;
            case CHOOSING_NAME:
                switch (General.random(0, 2)) {
                    case 0:
                        InterfaceHandler.clickInterface(Interfaces.get(558, 14));
                        break;
                    case 1:
                        InterfaceHandler.clickInterface(Interfaces.get(558, 15));
                        break;
                    case 2:
                        InterfaceHandler.clickInterface(Interfaces.get(558, 16));
                        break;
                }
                startWaitTime = System.currentTimeMillis();
                Timing.waitCondition(() -> Interfaces.get(558, 18).getActions() != null, 10000);
                Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
                break;
            case CONFIRMING_NAME:
                InterfaceHandler.clickInterface(Interfaces.get(558, 18));
                break;
            case SELECTING_CHARACTER:
                InterfaceHandler.clickInterface(Interfaces.get(269, 100));
                break;
        }
    }
}
