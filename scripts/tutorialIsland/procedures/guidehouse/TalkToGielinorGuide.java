package scripts.tutorialIsland.procedures.guidehouse;

import org.tribot.api.Timing;
import org.tribot.api2007.*;
import org.tribot.api2007.types.RSInterface;
import org.tribot.api2007.types.RSNPC;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.bbuu20_api.objhandlers.NPCHandler;
import scripts.bbuu20_api.util.Antiban;
import scripts.tutorialIsland.data.SubTask;
import scripts.tutorialIsland.data.Vars;

public class TalkToGielinorGuide extends MainFramework {
    @Override
    public String status() {
        return "Talking to Gielinor Guide";
    }

    @Override
    public boolean shouldProceed() {
        return Vars.get().currentSubTask == SubTask.TALKING_TO_GUIDE
                || Vars.get().currentSubTask == SubTask.SELECTING_GUIDE_OPTION
                || Vars.get().currentSubTask == SubTask.OPENING_SETTINGS_TAB;

    }

    @Override
    public void proceed() {
        long startWaitTime;
        RSInterface[] chatOptions = {Interfaces.get(219, 1, 1), Interfaces.get(219, 1, 2), Interfaces.get(219, 1, 3)};
        switch (Vars.get().currentSubTask) {
            case TALKING_TO_GUIDE:
                RSNPC[] gielinorGuide = NPCs.find("Gielinor guide");
                if (!NPCHandler.continueChatAndSleep())
                    NPCHandler.clickRSNPC(gielinorGuide);
                break;
            case SELECTING_GUIDE_OPTION:
                for (RSInterface chatOption : chatOptions) {
                    if (chatOption.getText().equals("I am an experienced player.")) {
                        startWaitTime = System.currentTimeMillis();
                        chatOption.click();
                        Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
                        break;
                    }
                }
                break;
            case OPENING_SETTINGS_TAB:
                GameTab.TABS.OPTIONS.open();
                startWaitTime = System.currentTimeMillis();
                Timing.waitCondition(GameTab.TABS.OPTIONS::isOpen, 3500);
                Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime)); //use an abc sleep
                break;
        }
    }
}