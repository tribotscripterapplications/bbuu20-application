package scripts.tutorialIsland.procedures.bank;

import org.tribot.api2007.GameTab;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.types.RSNPC;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.bbuu20_api.objhandlers.NPCHandler;
import scripts.tutorialIsland.data.SubTask;
import scripts.tutorialIsland.data.Vars;

public class HandleAdvisor extends MainFramework {
    @Override
    public String status() {
        return "Handle advisor";
    }

    @Override
    public boolean shouldProceed() {
        return Vars.get().currentSubTask == SubTask.TALKING_TO_ADVISER
                || Vars.get().currentSubTask == SubTask.OPENING_ACCOUNT_TAB;
    }

    @Override
    public void proceed() {
        if (Vars.get().currentSubTask == SubTask.TALKING_TO_ADVISER) {
            if (!NPCHandler.continueChatAndSleep()) {
                RSNPC[] adviser = NPCs.find("Account Guide");
                NPCHandler.clickRSNPC(adviser);
            }
        }
        if (Vars.get().currentSubTask == SubTask.OPENING_ACCOUNT_TAB) {
            if (!NPCHandler.continueChatAndSleep()) {
                GameTab.TABS.ACCOUNT.open();
            }
        }
    }
}
