package scripts.tutorialIsland.procedures.bank;

import org.tribot.api.Timing;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.Objects;
import org.tribot.api2007.types.RSObject;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.bbuu20_api.objhandlers.InterfaceHandler;
import scripts.bbuu20_api.objhandlers.NPCHandler;
import scripts.bbuu20_api.objhandlers.ObjectHandler;
import scripts.tutorialIsland.data.SubTask;
import scripts.tutorialIsland.data.Vars;

public class HandleBank extends MainFramework {
    private String statusUpdate = "Bank";

    @Override
    public String status() {
        return statusUpdate;
    }

    @Override
    public boolean shouldProceed() {
        return (Vars.get().currentSubTask == SubTask.OPENING_BANK
                || Vars.get().currentSubTask == SubTask.CLOSING_BANK
                || Vars.get().currentSubTask == SubTask.CLICKING_POLL_BOOTH
                || Vars.get().currentSubTask == SubTask.HANDLING_POLL_BOOTH
                || Vars.get().currentSubTask == SubTask.CLOSING_POLL_BOOTH);
    }

    @Override
    public void proceed() {
        if (Vars.get().currentSubTask == SubTask.OPENING_BANK) {
            Banking.openBank();
            Timing.waitCondition(Banking::isBankScreenOpen, 5000);
            return;
        }
        if (Vars.get().currentSubTask == SubTask.CLOSING_BANK) {
            Banking.close();
            Timing.waitCondition(() -> !Banking.isBankScreenOpen(), 5000);
        }
        if (Vars.get().currentSubTask == SubTask.CLICKING_POLL_BOOTH) {
            RSObject[] pollBooth = Objects.find(15, "Poll booth");
            ObjectHandler.clickRSObject(pollBooth);
            Timing.waitCondition(() -> !InterfaceHandler.isOnScreen(Interfaces.get(263, 1, 0)), 5000);
        }
        if (Vars.get().currentSubTask == SubTask.HANDLING_POLL_BOOTH) {
            if (!InterfaceHandler.clickInterface(Interfaces.get(193, 0, 2))) {
                NPCHandler.continueChatAndSleep();
            }
        }
        if (Vars.get().currentSubTask == SubTask.CLOSING_POLL_BOOTH) {
            InterfaceHandler.clickInterface(Interfaces.get(310, 2, 11));
        }
    }
}
