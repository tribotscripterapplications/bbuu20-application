package scripts.tutorialIsland;

import org.tribot.api.General;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.util.ThreadSettings;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.bbuu20_api.objhandlers.InterfaceHandler;
import scripts.dax_api.api_lib.DaxWalker;
import scripts.dax_api.api_lib.models.DaxCredentials;
import scripts.dax_api.api_lib.models.DaxCredentialsProvider;
import scripts.tutorialIsland.data.Vars;
import scripts.tutorialIsland.procedures.Traverse;
import scripts.tutorialIsland.procedures.bank.HandleAdvisor;
import scripts.tutorialIsland.procedures.bank.HandleBank;
import scripts.tutorialIsland.procedures.chapel.HandleChapel;
import scripts.tutorialIsland.procedures.combatarea.HandleCombatArea;
import scripts.tutorialIsland.procedures.combatarea.TalkToCombatGuide;
import scripts.tutorialIsland.procedures.guidehouse.CreateCharacter;
import scripts.tutorialIsland.procedures.guidehouse.TalkToGielinorGuide;
import scripts.tutorialIsland.procedures.kitchen.HandleKitchen;
import scripts.tutorialIsland.procedures.kitchen.TalkToChef;
import scripts.tutorialIsland.procedures.mine.HandleMiningArea;
import scripts.tutorialIsland.procedures.mine.TalkToMiningGuide;
import scripts.tutorialIsland.procedures.questhouse.TalkToQuestGuide;
import scripts.tutorialIsland.procedures.survivalarea.HandleSurvivalArea;
import scripts.tutorialIsland.procedures.survivalarea.TalkToSurvivalExpert;
import scripts.tutorialIsland.procedures.wizardhouse.HandleWizard;
import scripts.tutorialIsland.util.Calculations;

import java.util.ArrayList;

@ScriptManifest(
        authors = "bbuu20"                                             ,
        category = "Quests"                                            ,
        name = "Bbuu20's Tutorial Island"                        ,
        gameMode = 1                                                   ,
        version = 2.0                                                  ,
        description = "Bbuu20's Tutorial Island is finally back!" +
                " I have completely rewritten this script with " +
                "ABC implementation, " +
                "and can confidently say it is the best tutorial " +
                "island script across all botting clients. " +
                "More features " +
                "will be returning soon, such as gui and paint."       )

public class TutorialMain extends Script {
    private ArrayList<MainFramework> tasks = new ArrayList<>();


    @Override
    public void run() {
        DaxWalker.setCredentials(new DaxCredentialsProvider() {
            @Override
            public DaxCredentials getDaxCredentials() {
                return new DaxCredentials("sub_DPjXXzL5DeSiPf", "PUBLIC-KEY");
            }
        });

        //set up
        General.useAntiBanCompliance(true);
        ThreadSettings.get().setClickingAPIUseDynamic(true);
        newProcedures();

        while (true) {
            Vars.get().currentSubTask = Calculations.generateSubTask();
            carryOutProcedures();
            General.sleep(50); //sleep every 50 milliseconds so we don't burn the vps data center to the ground
        }
    }

    private void newProcedures() {
        //add an instance of each procedure to our tasks list
        tasks.add(new CreateCharacter());
        tasks.add(new TalkToGielinorGuide());
        tasks.add(new TalkToSurvivalExpert());
        tasks.add(new HandleSurvivalArea());
        tasks.add(new Traverse());
        tasks.add(new TalkToChef());
        tasks.add(new HandleKitchen());
        tasks.add(new TalkToQuestGuide());
        tasks.add(new TalkToMiningGuide());
        tasks.add(new HandleMiningArea());
        tasks.add(new TalkToCombatGuide());
        tasks.add(new HandleCombatArea());
        tasks.add(new HandleBank());
        tasks.add(new HandleAdvisor());
        tasks.add(new HandleChapel());
        tasks.add(new HandleWizard());
    }

    private void carryOutProcedures() {
        InterfaceHandler.clickInterface(Interfaces.get(162, 45)); //click the "Click here to continue" screen if it pops up at any point
        for (MainFramework task: tasks) {
            if (task.shouldProceed()) {
                System.out.print(task.status());
                task.proceed(); //if we should proceed, carry out the procedure
                General.sleep(50, 150);
                break;
            }
        }
    }
}