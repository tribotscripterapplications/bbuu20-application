package scripts.tutorialIsland.data;

import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;

public class Finals {

    /*
     RSAreas
    */

    public static final RSArea RATPIT = new RSArea(new RSTile[] {
        new RSTile(3094, 9517, 0),
        new RSTile(3094, 9519, 0),
        new RSTile(3096, 9522, 0),
        new RSTile(3098, 9523, 0),
        new RSTile(3099, 9524, 0),
        new RSTile(3101, 9526, 0),
        new RSTile(3103, 9526, 0),
        new RSTile(3104, 9525, 0),
        new RSTile(3106, 9524, 0),
        new RSTile(3107, 9523, 0),
        new RSTile(3108, 9523, 0),
        new RSTile(3109, 9523, 0),
        new RSTile(3110, 9522, 0),
        new RSTile(3110, 9521, 0),
        new RSTile(3111, 9520, 0),
        new RSTile(3111, 9519, 0),
        new RSTile(3111, 9518, 0),
        new RSTile(3110, 9517, 0),
        new RSTile(3110, 9516, 0),
        new RSTile(3110, 9515, 0),
        new RSTile(3109, 9514, 0),
        new RSTile(3107, 9513, 0),
        new RSTile(3107, 9512, 0),
        new RSTile(3106, 9511, 0),
        new RSTile(3105, 9510, 0),
        new RSTile(3102, 9510, 0)
    });

    public static final RSArea SURVIVAL_AREA = new RSArea(new RSTile(3101, 3097, 0), new RSTile(3104, 3094, 0));
    public static final RSArea KITCHEN_AREA = new RSArea(new RSTile(3078, 3086, 0), new RSTile(3073, 3083, 0));
    public static final RSArea QUEST_GUIDE_HOUSE = new RSArea(new RSTile(3089, 3119, 0), new RSTile(3080, 3125, 0));
    public static final RSArea MINING_AREA = new RSArea(new RSTile(3078, 9508, 0), new RSTile(3082, 9504, 0));
    public static final RSArea COMBAT_AREA = new RSArea(new RSTile(3104, 9509, 0), new RSTile(3106, 9507, 0));
    public static final RSArea BANK_AREA = new RSArea(new RSTile(3118, 3125, 0), new RSTile(3124, 3119, 0));
    public static final RSArea PRIEST_GUIDE_HOUSE = new RSArea(new RSTile(3120, 3110, 0), new RSTile(3128, 3103, 0));
    public static final RSArea ADVISER_GUIDE_ROOM = new RSArea(new RSTile(3125, 3125, 0), new RSTile(3129, 3123, 0));
    public static final RSArea WIZARD_HOUSE = new RSArea(new RSTile(3137, 3091, 0), new RSTile(3143, 3082, 0));
}