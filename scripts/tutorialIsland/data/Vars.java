package scripts.tutorialIsland.data;

public class Vars {
    private Vars() {}
    private static Vars vars = new Vars();
    public static Vars get() {
        return vars;
    }
    public SubTask currentSubTask; //will contain the current sub-task based on the game setting
    public int currentTaskCode; //will contain the current task code of the tutorial island setting
}