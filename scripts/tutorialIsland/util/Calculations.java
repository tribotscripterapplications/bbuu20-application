package scripts.tutorialIsland.util;

import org.tribot.api.General;
import org.tribot.api2007.*;
import scripts.bbuu20_api.objhandlers.InterfaceHandler;
import scripts.tutorialIsland.data.Finals;
import scripts.tutorialIsland.data.SubTask;

import static scripts.tutorialIsland.data.SubTask.*;

public class Calculations {

    public static char generateRandomChar() { //generate a random character for the name selection screen
        String alphaNumeric = "ABCDEF GHIJKL MNOPQR STUVWX YZ abcdef ghijkl mnopqr stuvwx yz 123456 7890 ";
        return alphaNumeric.charAt(General.random(0, 73));
    }
    public static SubTask generateSubTask() {
        //setting 281 is the game setting that will update at each major step of tutorial island.
        //for example: if Game.getSetting == 510, our player is on the banking step of tutorial island
        switch (Game.getSetting(281)) {
            case 1:
                if (!InterfaceHandler.isOnScreen(Interfaces.get(269, 100))) {
                    if (!InterfaceHandler.isOnScreen(Interfaces.get(558, 14))) {
                        if (InterfaceHandler.isOnScreen(Interfaces.get(558, 18))) {
                            if (Interfaces.get(558, 18).getActions() != null) {
                                return CONFIRMING_NAME;
                            }
                        }
                        if (!InterfaceHandler.isOnScreen(Interfaces.get(162, 44))) {
                            return OPENING_TEXT;
                        }
                        return TYPING_NAME;
                    }
                    return CHOOSING_NAME;
                }
                return SELECTING_CHARACTER;
            case 2:
            case 3:
            case 7:
                if (InterfaceHandler.isOnScreen(Interfaces.get(263, 1, 0))) {
                    if (Interfaces.get(263, 1, 0).getText().contains("This will display your options menu.")) {
                        return OPENING_SETTINGS_TAB;
                    }
                }
                if (InterfaceHandler.isOnScreen(Interfaces.get(219, 1, 1))) {
                    return SELECTING_GUIDE_OPTION;
                }
                return TALKING_TO_GUIDE;
            case 10:
                return WALKING_TO_SURVIVAL_AREA;
            case 20:
            case 60:
                return TALKING_TO_SURVIVAL_EXPERT;
            case 30:
                return OPENING_INVENTORY;
            case 40:
                return FISHING;
            case 50:
                return OPENING_STATS_TAB;
            case 70:
                return CHOPPING_TREE;
            case 80:
                return MAKING_FIRE;
            case 90:
                return COOKING_SHRIMPS;
            case 120:
            case 130:
                return WALKING_TO_KITCHEN;
            case 140:
                return TALKING_TO_CHEF;
            case 150:
                return MAKING_DOUGH;
            case 160:
                return COOKING_BREAD;
            case 170:
            case 200:
                return WALKING_TO_QUEST_GUIDE;
            case 220:
            case 230:
            case 240:
                if (Game.getSetting(281) == 230) {
                    if (NPCChat.getMessage() == null)
                        return OPENING_QUESTS_TAB;
                }
                return TALKING_TO_QUEST_GUIDE;
            case 250:
                return WALKING_TO_MINING_AREA;
            case 260:
            case 330:
                return TALKING_TO_MINING_GUIDE;
            case 270:
            case 300:
                return MINING_TIN;
            case 310:
                return MINING_COPPER;
            case 320:
                return SMELTING_ORES;
            case 340:
            case 350:
                return SMITHING_DAGGER;
            case 360:
                return WALKING_TO_COMBAT_AREA;
            case 370:
            case 470:
                if (InterfaceHandler.isOnScreen(Interfaces.get(263, 1, 0)))
                    if (Interfaces.get(263, 1, 0).getText().contains("It's time to slay some rats! To attack"))
                        return CLICKING_RAT;
                    if (Finals.RATPIT.contains(Player.getPosition()))
                        return LEAVING_RAT_PIT;
                return TALKING_TO_COMBAT_GUIDE;
            case 390:
                if (GameTab.TABS.EQUIPMENT.isOpen())
                    return TALKING_TO_COMBAT_GUIDE;
                return OPENING_EQUIPMENT_TAB;
            case 400:
                return OPENING_ARMORY;
            case 405:
                return EQUIPPING_DAGGER;
            case 410:
                if (!InterfaceHandler.isOnScreen(Interfaces.get(84, 4)))
                    return TALKING_TO_COMBAT_GUIDE;
                return CLOSING_ARMORY;
            case 420:
                return EQUIPPING_WEAPONS;
            case 430:
                return OPENING_COMBAT_TAB;
            case 440:
                return ENTERING_RAT_PIT;
            case 450:
                if (Finals.RATPIT.contains(Player.getPosition()))
                    return CLICKING_RAT;
                return ENTERING_RAT_PIT;
            case 460:
            case 490:
                return IN_COMBAT;
            case 480:
                if (Inventory.getCount("Shortbow") > 0 || Inventory.getCount("Bronze arrow") > 0) {
                    return EQUIPPING_BOW_AND_ARROW;
                }
                return CLICKING_RAT;
            case 500:
                return WALKING_TO_BANK;
            case 510:
                if (!Finals.BANK_AREA.contains(Player.getPosition()))
                    return WALKING_TO_BANK;
                return OPENING_BANK;
            case 520:
                if (Banking.isBankScreenOpen())
                    return CLOSING_BANK;
                if (InterfaceHandler.isOnScreen(Interfaces.get(263, 1, 0)))
                    return CLICKING_POLL_BOOTH;
                if (InterfaceHandler.isOnScreen(Interfaces.get(193, 0, 2)))
                    return HANDLING_POLL_BOOTH;
                if (InterfaceHandler.isOnScreen(Interfaces.get(310, 2, 11)))
                    return CLOSING_POLL_BOOTH;
            case 525:
                if (InterfaceHandler.isOnScreen(Interfaces.get(263, 1, 0)))
                    if (Interfaces.get(263, 1, 0).getText().contains("Polls are run periodically to let the"))
                        return WALKING_TO_ADVISER;
            case 530:
            case 532:
                return TALKING_TO_ADVISER;
            case 531:
                return OPENING_ACCOUNT_TAB;
            case 540:
            case 550:
            case 570:
            case 600:
                return WALKING_AND_TALKING_TO_PRIEST;
            case 560:
                return OPENING_PRAYER_TAB;
            case 580:
                return OPENING_FRIENDS_TAB;
            case 610:
            case 620:
            case 640:
                if (!Finals.WIZARD_HOUSE.contains(Player.getPosition()))
                    return WALKING_TO_WIZARD;
                return TALKING_TO_WIZARD;
            case 630:
                return OPENING_MAGIC_TAB;
            case 650:
                return SELECTING_SPELL;
            case 670:
                if (InterfaceHandler.isOnScreen(Interfaces.get(193, 0, 2)))
                    return CLOSING_TIP;
                if (InterfaceHandler.isOnScreen(Interfaces.get(219, 1, 3)))
                    if ((Interfaces.get(219, 1, 3).getText().equals("No, I'm not planning to do that.")))
                        return SELECTING_WIZARD_OPTION_THREE;
                if (InterfaceHandler.isOnScreen(Interfaces.get(219, 1, 1)))
                    return SELECTING_WIZARD_OPTION_ONE;
                return TALKING_TO_WIZARD;

            default:
                return null;
        }
    }
}
