package scripts.firemaker;

import org.tribot.api.General;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.dax_api.api_lib.DaxWalker;
import scripts.dax_api.api_lib.models.DaxCredentials;
import scripts.dax_api.api_lib.models.DaxCredentialsProvider;
import scripts.firemaker.data.Vars;
import scripts.firemaker.framework.procedures.Bank;
import scripts.firemaker.framework.procedures.MakeFire;
import scripts.firemaker.framework.procedures.Traverse;
import scripts.firemaker.graphics.FXMLString;
import scripts.firemaker.graphics.GUI;

import java.util.ArrayList;

@ScriptManifest(
        name = "Local Bbuu20's Firemaker",
        authors = "bbuu20",
        gameMode = 1,
        version = 1.0,
        description = "Makes fires from any log. Stand where you want the bot to start making fires!",
        category = "Firemaking"
)

public class FireMaker extends Script {
    private ArrayList<MainFramework> procedures = new ArrayList<>();
    private GUI gui;

    @Override
    public void run() {
        DaxWalker.setCredentials(new DaxCredentialsProvider() {
            @Override
            public DaxCredentials getDaxCredentials() {
                return new DaxCredentials("sub_DPjXXzL5DeSiPf", "PUBLIC-KEY");
            }
        });

        General.useAntiBanCompliance(true);

        // Run GUI (fxml created with SceneBuilder)
        gui = new GUI(FXMLString.get);
        gui.show();
        while (gui.isOpen()) {
            General.sleep(300, 500);
        }
        Vars.get().currentLogID = Vars.get().selectedLogIDs.get(Vars.get().selectedLogIDs.size() - 1);

        newProcedures();

        while (true) {
            carryOutProcedures();
            General.sleep(10);
        }
    }

    private void newProcedures() {
        procedures.add(new Bank());
        procedures.add(new MakeFire());
        procedures.add(new Traverse());
    }

    private void carryOutProcedures() {
        for (MainFramework procedure : procedures) {
            if (procedure.shouldProceed()) {
                System.out.println(procedure.status());
                procedure.proceed();
                break;
            }
        }
    }
}
