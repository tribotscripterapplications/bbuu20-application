package scripts.firemaker.data;

import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSTile;

import java.util.ArrayList;
import java.util.List;

public class Vars {
    private Vars()  {}
    private static Vars vars = new Vars();
    public static Vars get() {
        return vars;
    }

    public List<Integer> selectedLogIDs = new ArrayList<>();

    public RSItem[] logItem;

    public RSObject[] fires;

    public RSTile playerPosition;

    public int currentLogID;

    public boolean isOnStartingTile = true;
    public boolean strikeTinderbox = true;


}
