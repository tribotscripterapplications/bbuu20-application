package scripts.firemaker.data;

import org.tribot.api2007.Inventory;
import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSTile;

public class Finals {

    private static final int TINDERBOXID = 590;

    public static final int[] LOG_IDS = {
            1511, //logs
            1521, //oak logs
            1519, //willow logs
            1517, //maple logs
            1515, //yew logs
            1513, //magic logs
            19669, //redwood logs
    };

    public static final RSItem[] TINDERBOX_ITEM = Inventory.find(TINDERBOXID);

    public static final RSTile STARTING_POSITION = Player.getPosition();
}
