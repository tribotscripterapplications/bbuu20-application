package scripts.firemaker.framework.procedures;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.*;
import org.tribot.api2007.types.RSGroundItem;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSTile;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.bbuu20_api.objhandlers.GroundItemHandler;
import scripts.bbuu20_api.objhandlers.ItemHandler;
import scripts.bbuu20_api.util.Antiban;
import scripts.firemaker.data.Finals;
import scripts.firemaker.data.Vars;

public class MakeFire extends MainFramework {

    private long startedFiremaking, lastFiremakingWaitTime, averageFiremakingWaitTime, totalFiremakingWaitTime, totalFiremakingInstances;

    private void updateAntibanStatistics(long startedFiremaking) {
        this.lastFiremakingWaitTime = System.currentTimeMillis() - startedFiremaking;
        this.totalFiremakingInstances++;
        this.totalFiremakingWaitTime += lastFiremakingWaitTime;
        this.averageFiremakingWaitTime = totalFiremakingWaitTime / totalFiremakingInstances;
    }

    @Override
    public String status() {
        return "Fire Making";
    }

    @Override
    public boolean shouldProceed() {
        for (int logID : Finals.LOG_IDS) {
            if (Inventory.getCount(logID) > 0)
                return Vars.get().isOnStartingTile;
        }
        return false;
    }

    @Override
    public void proceed() {
        Vars.get().fires = Objects.findNearest(1, 26185);
        for (RSObject fire : Vars.get().fires) {
            if (fire.getPosition().equals(Player.getPosition())) {
                Walking.walkTo(new RSTile(Player.getPosition().getX(), Player.getPosition().getY() - 1, Player.getPosition().getPlane()));
            }
        }
        Vars.get().playerPosition = Player.getPosition();
        Vars.get().logItem = Inventory.find(Vars.get().currentLogID);

        if (Vars.get().strikeTinderbox) {
            ItemHandler.clickRSItem(Finals.TINDERBOX_ITEM);
            Vars.get().strikeTinderbox = false;
        }

        ItemHandler.clickRSItem(Vars.get().logItem);

        if (Inventory.getCount(Vars.get().currentLogID) > 0) {
            Vars.get().logItem = Inventory.find(Vars.get().currentLogID);

            ItemHandler.clickRSItem(Finals.TINDERBOX_ITEM);

            ItemHandler.hoverRSItem(Vars.get().logItem);
        }

        startedFiremaking = System.currentTimeMillis();

        while (Player.getPosition().equals(Vars.get().playerPosition)) {
            Antiban.get().idleTimedActions();
            if (averageFiremakingWaitTime != 0) {
                if (System.currentTimeMillis() - startedFiremaking > (averageFiremakingWaitTime * 2)) {
                    RSGroundItem[] logs = GroundItems.find(Vars.get().currentLogID);
                    GroundItemHandler.clickRSGroundItem(logs);
                }
            }
            else if (System.currentTimeMillis() - startedFiremaking > General.random(5000, 8000)) {
                RSGroundItem[] logs = GroundItems.find(Vars.get().currentLogID);
                GroundItemHandler.clickRSGroundItem(logs);
                General.sleep(General.random(300, 500));
                Timing.waitCondition(() -> !Player.getPosition().equals(Vars.get().playerPosition), 15000);
                Vars.get().strikeTinderbox = true;
            }
        }
        Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startedFiremaking));
        Antiban.get().generateSupportingTrackerInfo((int)averageFiremakingWaitTime);
    }
}
