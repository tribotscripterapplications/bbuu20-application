package scripts.firemaker.framework.procedures;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Player;
import org.tribot.api2007.Walking;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.bbuu20_api.util.Antiban;
import scripts.firemaker.data.Finals;
import scripts.firemaker.data.Vars;

public class Traverse extends MainFramework {
    private long startWaitTime;

    @Override
    public String status() {
        return "Traversing";
    }

    @Override
    public boolean shouldProceed() {
        return Inventory.getCount(Vars.get().currentLogID) > 0 && !Finals.STARTING_POSITION.equals(Player.getPosition());
    }

    @Override
    public void proceed() {
        while (!Finals.STARTING_POSITION.equals(Player.getPosition())) {
            startWaitTime = System.currentTimeMillis();
            Walking.blindWalkTo(Finals.STARTING_POSITION);
            Timing.waitCondition(() -> Player.getPosition().equals(Finals.STARTING_POSITION), General.random(2500, 4000));
            Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - startWaitTime));
        }
        Vars.get().isOnStartingTile = true;
    }
}