package scripts.firemaker.framework.procedures;

import org.tribot.api.Timing;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Inventory;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.bbuu20_api.objhandlers.WalkingHandler;
import scripts.firemaker.data.Finals;
import scripts.firemaker.data.Vars;

public class Bank extends MainFramework {

    @Override
    public String status() {
        return "Banking";
    }

    @Override
    public boolean shouldProceed() {
        for (int logID : Finals.LOG_IDS) {
            if (Inventory.getCount(logID) > 0)
                return false;
        }
        return true;
    }

    @Override
    public void proceed() {
        switch (generateSubtasks()) {
            case WALKING_TO_BANK:
                WalkingHandler.walkToBankAndSleep();
                break;
            case OPENING_BANK:
                Banking.openBank();
                Timing.waitCondition(Banking::isBankScreenOpen, 5000);
                break;
            case WITHDRAW_LOGS:
                Banking.withdraw(28, Vars.get().currentLogID);
                if (Banking.find(Vars.get().currentLogID).length == 0
                        && Vars.get().selectedLogIDs.size() > 1
                        && !Inventory.isFull()
                        && Inventory.getCount(Vars.get().selectedLogIDs.size() - 1) == 0) { //if we are out of the current log, we have another log selected, and our inventory is full
                    Vars.get().selectedLogIDs.remove(Vars.get().selectedLogIDs.size() - 1); //change the current log to the next best available
                }
                Vars.get().currentLogID = Vars.get().selectedLogIDs.get(Vars.get().selectedLogIDs.size() - 1);

                Vars.get().isOnStartingTile = false;
                Vars.get().strikeTinderbox = true;
                Timing.waitCondition(() -> Inventory.getCount(Vars.get().currentLogID) > 0, 5000);
                break;
        }
    }

    private enum subtasks {
        WALKING_TO_BANK,
        OPENING_BANK,
        WITHDRAW_LOGS
    }

    private subtasks generateSubtasks() {
        if (!Banking.isInBank()) {
            return subtasks.WALKING_TO_BANK;
        }
        if (!Banking.isBankScreenOpen()) {
            return subtasks.OPENING_BANK;
        }
        return subtasks.WITHDRAW_LOGS;
    }
}
