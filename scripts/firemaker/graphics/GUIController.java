package scripts.firemaker.graphics;

import java.net.URL;
import java.util.ResourceBundle;

import com.allatori.annotations.DoNotRename;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import org.tribot.api.General;
import scripts.firemaker.data.Vars;


/**
 * Credits to laniax for the base JavaFX GUI + Controller in his API located at https://github.com/Laniax/LanAPI/blob/master/core/gui/
 */

@DoNotRename 
public class GUIController implements Initializable {

	private GUI gui;

	@DoNotRename @FXML
	private ImageView fireGif;

	@DoNotRename @FXML
	private ImageView logImage;

	@DoNotRename @FXML
	private ImageView oakImage;

	@DoNotRename @FXML
	private ImageView willowImage;

	@DoNotRename @FXML
	private ImageView mapleImage;

	@DoNotRename @FXML
	private ImageView yewImage;

	@DoNotRename @FXML
	private ImageView magicImage;

	@DoNotRename @FXML
	private ImageView redwoodImage;

	@DoNotRename @FXML
	private AnchorPane anchorPane;

	@DoNotRename @FXML
	private Button startButton;

	@DoNotRename @FXML
	private CheckBox addLogsButton;

	@DoNotRename @FXML
	private CheckBox addOakButton;

	@DoNotRename @FXML
	private CheckBox addWillowButton;

	@DoNotRename @FXML
	private CheckBox addMapleButton;

	@DoNotRename @FXML
	private CheckBox addYewButton;

	@DoNotRename @FXML
	private CheckBox addMagicButton;

	@DoNotRename @FXML
	private CheckBox addRedwoodButton;

	@DoNotRename @FXML
	public void startScriptPressed() {
		if (addLogsButton.isSelected()) {
			Vars.get().selectedLogIDs.add(1511);
		}
		if (addOakButton.isSelected()) {
			Vars.get().selectedLogIDs.add(1521);
		}
		if (addWillowButton.isSelected()) {
			Vars.get().selectedLogIDs.add(1519);
		}
		if (addMapleButton.isSelected()) {
			Vars.get().selectedLogIDs.add(1517);
		}
		if (addYewButton.isSelected()) {
			Vars.get().selectedLogIDs.add(1515);
		}
		if (addMagicButton.isSelected()) {
			Vars.get().selectedLogIDs.add(1513);
		}
		if (addRedwoodButton.isSelected()) {
			Vars.get().selectedLogIDs.add(19669);
		}
		this.gui.close();
	}
	
	public void setGUI(GUI gui) {
		this.gui = gui;
	}
	
	public GUI getGUI() {
		return this.gui;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		Image firesGif = new Image("https://i.imgur.com/d3mRjZv.gif");
		Image logsImage = new Image("https://i.imgur.com/ZkN7kTw.png");
		Image oaksImage = new Image("https://i.imgur.com/BBXLsca.png");
		Image willowsImage = new Image("https://i.imgur.com/OsXMKfJ.png");
		Image maplesImage = new Image("https://i.imgur.com/d4WTK92.png");
		Image yewsImage = new Image("https://i.imgur.com/A4Kilvr.png");
		Image magicsImage = new Image("https://i.imgur.com/qG1MSzG.png");
		Image redwoodsImage = new Image("https://i.imgur.com/jf2Ini3.png");

		fireGif.setImage(firesGif);
		logImage.setImage(logsImage);
		oakImage.setImage(oaksImage);
		willowImage.setImage(willowsImage);
		mapleImage.setImage(maplesImage);
		yewImage.setImage(yewsImage);
		magicImage.setImage(magicsImage);
		redwoodImage.setImage(redwoodsImage);
	}
}
