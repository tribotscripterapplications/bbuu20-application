package scripts.firemaker.graphics;

public class FXMLString {

	public static String get = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
			"\n" +
			"<?import javafx.scene.control.Button?>\n" +
			"<?import javafx.scene.control.CheckBox?>\n" +
			"<?import javafx.scene.control.Label?>\n" +
			"<?import javafx.scene.layout.AnchorPane?>\n" +
			"<?import javafx.scene.text.Font?>\n" +
			"\n" +
			"<?import javafx.scene.image.ImageView?>\n" +
			"<AnchorPane fx:id=\"anchorPane\" maxHeight=\"-Infinity\" maxWidth=\"-Infinity\" minHeight=\"-Infinity\" minWidth=\"-Infinity\" prefHeight=\"300.0\" prefWidth=\"200.0\" xmlns=\"http://javafx.com/javafx/11.0.1\" xmlns:fx=\"http://javafx.com/fxml/1\" fx:controller=\"scripts.firemaker.graphics.GUIController\">\n" +
			"   <children>\n" +
			"      <Button fx:id=\"startButton\" prefHeight=\"20.0\" prefWidth=\"195.0\" layoutX=\"2.5\" layoutY=\"226.0\" mnemonicParsing=\"false\" onAction=\"#startScriptPressed\" text=\"Start Firemaking\">\n" +
			"         <graphic>\n" +
			"            <ImageView fx:id=\"fireGif\" scaleX=\"0.5\" scaleY=\"0.5\" />\n" +
			"         </graphic>\n" +
			"      </Button>\n" +
			"      <Label layoutX=\"6.0\" layoutY=\"6.0\" text=\"Select one or more type of log:\">\n" +
			"         <font>\n" +
			"            <Font size=\"13.0\" />\n" +
			"         </font>\n" +
			"      </Label>\n" +
			"      <CheckBox fx:id=\"addLogsButton\" layoutX=\"26.0\" layoutY=\"32.0\" mnemonicParsing=\"false\" text=\"Logs\">\n" +
			"         <graphic>\n" +
			"            <ImageView fx:id=\"logImage\"/>\n" +
			"         </graphic>\n" +
			"      </CheckBox>\n" +
			"      <CheckBox fx:id=\"addOakButton\" layoutX=\"26.0\" layoutY=\"57.0\" mnemonicParsing=\"false\" text=\"Oak Logs\">\n" +
			"         <graphic>\n" +
			"            <ImageView fx:id=\"oakImage\"/>\n" +
			"         </graphic>\n" +
			"      </CheckBox>\n" +
			"      <CheckBox fx:id=\"addWillowButton\" layoutX=\"26.0\" layoutY=\"82.0\" mnemonicParsing=\"false\" text=\"Willow Logs\">\n" +
			"         <graphic>\n" +
			"            <ImageView fx:id=\"willowImage\"/>\n" +
			"         </graphic>\n" +
			"      </CheckBox>\n" +
			"      <CheckBox fx:id=\"addMapleButton\" layoutX=\"26.0\" layoutY=\"107.0\" mnemonicParsing=\"false\" text=\"Maple Logs\">\n" +
			"         <graphic>\n" +
			"            <ImageView fx:id=\"mapleImage\"/>\n" +
			"         </graphic>\n" +
			"      </CheckBox>\n" +
			"      <CheckBox fx:id=\"addYewButton\" layoutX=\"26.0\" layoutY=\"132.0\" mnemonicParsing=\"false\" text=\"Yew Logs\">\n" +
			"         <graphic>\n" +
			"            <ImageView fx:id=\"yewImage\"/>\n" +
			"         </graphic>\n" +
			"      </CheckBox>\n" +
			"      <CheckBox fx:id=\"addMagicButton\" layoutX=\"26.0\" layoutY=\"159.0\" mnemonicParsing=\"false\" text=\"Magic Logs\">\n" +
			"         <graphic>\n" +
			"            <ImageView fx:id=\"magicImage\"/>\n" +
			"         </graphic>\n" +
			"      </CheckBox>\n" +
			"      <CheckBox fx:id=\"addRedwoodButton\" layoutX=\"26.0\" layoutY=\"184.0\" mnemonicParsing=\"false\" text=\"Redwood Logs\">\n" +
			"         <graphic>\n" +
			"            <ImageView fx:id=\"redwoodImage\"/>\n" +
			"         </graphic>\n" +
			"      </CheckBox>\n" +
			"   </children>\n" +
			"</AnchorPane>";

}