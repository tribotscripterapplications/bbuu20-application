package scripts.safeminer;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Login;
import org.tribot.api2007.Objects;
import org.tribot.api2007.Projection;
import org.tribot.api2007.Skills;
import org.tribot.api2007.types.RSTile;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Painting;
import scripts.bbuu20_api.objhandlers.ObjectHandler;
import scripts.bbuu20_api.util.Antiban;
import scripts.dax_api.api_lib.DaxWalker;
import scripts.dax_api.api_lib.models.DaxCredentials;
import scripts.dax_api.api_lib.models.DaxCredentialsProvider;
import scripts.safeminer.data.Vars;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.safeminer.procedures.Bank;
import scripts.safeminer.procedures.DropInventory;
import scripts.safeminer.procedures.Mine;
import scripts.safeminer.procedures.Traverse;
import scripts.safeminer.graphics.gui.SwingUI;
import scripts.safeminer.util.Calculations;

import java.awt.*;
import java.text.DecimalFormat;
import java.util.ArrayList;

@ScriptManifest(
        name = "Bbuu20's Smart Miner",
        authors = "bbuu20",
        gameMode = 1,
        version = 1.5,
        description = "Full ABC2 Level 10 implementation! " +
                "Mines all ores at all locations, with banking" +
                "Uses a grid to select which rocks to mine" +
                "Supports banking and powermining at all locations!",
        category = "Mining"
)

public class SafeMiner extends Script implements Painting {
    private ArrayList<MainFramework> procedures = new ArrayList<>();
    private DecimalFormat df = new DecimalFormat("#.##");

    private static final long startTime = System.currentTimeMillis();
    private int startLvl = Skills.getActualLevel(Skills.SKILLS.MINING);
    private int startXP = Skills.getXP(Skills.SKILLS.MINING);

    private Font font = new Font("Verdana", Font.BOLD, 14);

    @Override
    public void run() {
        DaxWalker.setCredentials(new DaxCredentialsProvider() {
            @Override
            public DaxCredentials getDaxCredentials() {
                return new DaxCredentials("sub_DPjXXzL5DeSiPf", "PUBLIC-KEY");
            }
        });

        General.useAntiBanCompliance(true); //use abc!

        newProcedures(); //add new procedures to our list

        new SwingUI().drawUI(); //bring up the gui

        Vars.get().oreToMineIDS = Calculations.oreStringToIDs(Vars.get().oreToMine); //update the oreToMineIDS

        while (Login.getLoginState().equals(Login.STATE.INGAME)) {
            carryOutProcedures();
            General.sleep(1);
        }
    }
    
    private void newProcedures() {
        procedures.add(new Mine());
        procedures.add(new Traverse());
        procedures.add(new DropInventory());
        procedures.add(new Bank());
    }
    
    private void carryOutProcedures() {
        if (Vars.get().currentRock == null) {
            if (!Vars.get().timerStarted) {
                Vars.get().idleTimer = System.currentTimeMillis();
            }
            Vars.get().currentRock = Calculations.findNextRock();
        }
        else {
            Antiban.get().generateAndSleep((int)(System.currentTimeMillis() - Vars.get().idleTimer));
        }
        for (MainFramework procedure : procedures) {
            if (procedure.shouldProceed()) {
                System.out.println(procedure.status());
                procedure.proceed();
                break;
            }
        }
    }

    @Override
    public void onPaint(Graphics g) {
        long timeRan = System.currentTimeMillis() - startTime;
        int currentLvl = Skills.getActualLevel(Skills.SKILLS.MINING);
        int gainedLvl = currentLvl - startLvl;
        double gainedXP = Skills.getXP(Skills.SKILLS.MINING) - startXP;
        int xpToLevel = Skills.getXPToNextLevel(Skills.SKILLS.MINING);
        double xpPerHour = ((gainedXP * 3600) / timeRan);

        g.setFont(font);
        g.setColor(Color.WHITE);

        g.drawString("Runtime: " + Timing.msToString(timeRan), 0, 20);
        g.drawString("Current lvl: " + currentLvl + " (+" + gainedLvl + ")", 0, 40);
        g.drawString("XP Gained: " + gainedXP, 0, 60);
        g.drawString("XP To Next Level: " + xpToLevel, 0, 80);
        g.drawString("XP/H: " + df.format(xpPerHour) + "k", 0, 100);

        if (Vars.get().shouldPaintGrid) {
            g.setColor(Color.CYAN);
            for (RSTile tile : Calculations.getOnScreenArea().getAllTiles()) {
                if (tile.isOnScreen()) {
                    g.drawPolygon(Projection.getTileBoundsPoly(tile, 0));
                }
            }
            for (RSTile tile : Vars.get().paintedTiles) {
                g.fillPolygon(Projection.getTileBoundsPoly(tile, 0));
            }
        }
    }
}
