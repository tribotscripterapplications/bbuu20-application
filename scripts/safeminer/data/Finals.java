package scripts.safeminer.data;

public class Finals {

    public static final int[] PICK_IDS =
    {
        1265,   //bronze pick
        1267,   //iron pick
        1269,   //steel pick
        12297,  //black pick
        1273,   //mithril pick
        1271,   //adamant pick
        1275,   //rune pick
        11920   //dragon pick
    };
    public static final int[][] ROCK_IDS =
    {
        {11360, 11361},     //tin rock
        {10943, 11161},     //copper rock
        {11362, 11363},     //clay rock
        {11364, 11365},     //iron rock
        {11366, 11367},     //coal rock
        {11368, 11369},     //silver rock
        {11370, 11371},     //gold rock
        {11372, 11373},     //mithril rock
        {11374, 11375},     //adamantite rock
        {11376, 11377}      //runite rock
    };
}