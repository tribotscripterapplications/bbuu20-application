package scripts.safeminer.data;

import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSTile;

import java.util.ArrayList;
import java.util.List;

public class Vars {
    private Vars() {}
    private static Vars vars = new Vars();
    public static Vars get() {
        return vars;
    }
    /*
     Bools
    */
    public boolean shouldBank;
    public boolean shouldPaintGrid = true;
    public boolean shouldHop = false;
    public boolean timerStarted = false;

    /*
     Strings
    */
    public String oreToMine = "";

    /*
     Ints
    */
    public int miningRadius = 15;
    public int[] oreToMineIDS;

    /*
     Longs
    */
    public long idleTimer;

    /*
    RSTiles
    */
    public RSTile previousTile = Player.getPosition();
    public RSTile currentRockTile = null;
    public List<RSTile> paintedTiles = new ArrayList<>();

    /*
     RSObjects
    */
    public RSObject currentRock = null;
}
