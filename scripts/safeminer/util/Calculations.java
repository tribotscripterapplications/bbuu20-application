package scripts.safeminer.util;

import org.tribot.api.Timing;
import org.tribot.api.input.Mouse;
import org.tribot.api2007.Objects;
import org.tribot.api2007.Player;
import org.tribot.api2007.Projection;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSTile;
import scripts.safeminer.data.Finals;
import scripts.safeminer.data.Vars;

import java.awt.*;

public class Calculations {

    public static RSArea getOnScreenArea() {
        return new RSArea(
                new RSTile(
                        Player.getPosition().getX() + 15,
                        Player.getPosition().getY() + 15),
                new RSTile(
                        Player.getPosition().getX() - 15,
                        Player.getPosition().getY() - 15));
    }
    public static void setUpGrid() {
        for (RSTile tile : getOnScreenArea().getAllTiles()) {
            if (tile.isOnScreen()) {
                if (screenToTile(Mouse.getPos()) != null
                        && tile.getPosition().equals(screenToTile(Mouse.getPos()))
                        && Timing.waitCrosshair(1) == 2) {
                    for (RSTile checkTile : Vars.get().paintedTiles) {
                        if (checkTile.getPosition().equals(tile)) {
                            Vars.get().paintedTiles.remove(tile);
                            Timing.waitCondition(() -> Timing.waitCrosshair(100) == 0, 1000);
                            return;
                        }
                    }
                    Vars.get().paintedTiles.add(tile);
                    Timing.waitCondition(() -> Timing.waitCrosshair(100) == 0, 1000);
                }
            }
        }
    }
    private static RSTile screenToTile(Point p) {
        if (!Projection.isInViewport(p)) return null;
        RSTile pos = Player.getPosition();
        for (int x = pos.getX() - 12; x < pos.getX() + 12; x++) {
            for (int y = pos.getY() - 12; y < pos.getY() + 12; y++) {
                RSTile tile = new RSTile(x, y, pos.getPlane());
                Polygon bounds = Projection.getTileBoundsPoly(tile, 0);
                if (bounds != null && bounds.contains(p)) return tile;
            }
        }
        return null;
    }
    public static int[] oreStringToIDs(String ore) {
        switch (ore) {
            case "Tin ore":
                return Finals.ROCK_IDS[0];
            case "Copper ore":
                return Finals.ROCK_IDS[1];
            case "Clay":
                return Finals.ROCK_IDS[2];
            case "Iron ore":
                return Finals.ROCK_IDS[3];
            case "Coal":
                return Finals.ROCK_IDS[4];
            case "Silver ore":
                return Finals.ROCK_IDS[5];
            case "Gold ore":
                return Finals.ROCK_IDS[6];
            case "Mithril ore":
                return Finals.ROCK_IDS[7];
            case "Adamantite ore":
                return Finals.ROCK_IDS[8];
            case "Runite ore":
                return Finals.ROCK_IDS[9];
        }
        return new int[0];
    }
    public static RSObject findNextRock() {
        int[] rockIDS = Vars.get().oreToMineIDS;
        if (rockIDS.length > 0) {
            RSObject[] rocks = Objects.findNearest(Vars.get().miningRadius, rockIDS[0], rockIDS[1]);
            if (rocks != null) {
                for (RSObject rock : rocks) {
                    if (Vars.get().paintedTiles == null) { //if the user didn't select any specific rock
                        if (rock != Vars.get().currentRock)
                            return rock; //return the next rock of our type that isn't the current rock
                        break;
                    }
                    for (RSTile tile : Vars.get().paintedTiles) {
                        if (rock.getPosition().equals(tile)) {
                            if (rock != Vars.get().currentRock)
                                return rock;
                        }
                    }
                }
            }
        }
        return null;
    }
    public static boolean rockIsDepleted() {
        int[] rockIDS = Vars.get().oreToMineIDS;
        if (rockIDS.length > 0 && Player.getAnimation() != -1) {
            RSObject[] rocks = Objects.findNearest(Vars.get().miningRadius, rockIDS[0], rockIDS[1]);
            for (RSObject rock : rocks) {
                if (rock.getPosition().equals(Vars.get().currentRockTile)) {
                    return false;
                }
            }
        }
        return true;
    }
}
