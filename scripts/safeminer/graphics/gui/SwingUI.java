package scripts.safeminer.graphics.gui;

import org.tribot.api.General;
import scripts.safeminer.data.Vars;
import scripts.safeminer.util.Calculations;

import javax.swing.*;
import java.awt.*;


public class SwingUI {

    public void drawUI() {
        JFrame frame = new JFrame("SafeMiner");

        JCheckBox banking = new JCheckBox("Banking?");

        JButton startButton = new JButton("Start");

        JLabel tips = new JLabel("<html>Help: Start where you want your player to stand," +
                " and highlight the rocks you want to mine.</html>");

        JComboBox<String> oresBox = new JComboBox<>();
        getOres(oresBox);

        frame.setSize(250, 150);
        frame.setLocationRelativeTo(null);

        frame.add(oresBox, BorderLayout.NORTH);
        frame.add(banking, BorderLayout.WEST);
        frame.add(startButton, BorderLayout.SOUTH);
        frame.add(tips, BorderLayout.CENTER);

        frame.setVisible(true);

        while (frame.isVisible()) {
            frame.toFront();
            Calculations.setUpGrid();

            if (startButton.getModel().isPressed()) {
                Vars.get().shouldBank = banking.getModel().isSelected();
                Vars.get().oreToMine = oresBox.getModel().getSelectedItem().toString();
                frame.setVisible(false);
                Vars.get().shouldPaintGrid = false;
                return;
            }

            General.sleep(1);
        }
        throw new RuntimeException("GUI Closed");
    }
    private void getOres(JComboBox<String> oresBox) {
        oresBox.addItem("Copper ore");
        oresBox.addItem("Tin ore");
        oresBox.addItem("Clay");
        oresBox.addItem("Iron ore");
        oresBox.addItem("Coal");
        oresBox.addItem("Silver ore");
        oresBox.addItem("Gold ore");
        oresBox.addItem("Mithril ore");
        oresBox.addItem("Adamantite ore");
        oresBox.addItem("Runite ore");
    }
}
