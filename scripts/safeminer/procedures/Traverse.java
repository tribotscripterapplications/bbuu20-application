package scripts.safeminer.procedures;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.*;
import scripts.bbuu20_api.objhandlers.WalkingHandler;
import scripts.safeminer.data.Vars;
import scripts.bbuu20_api.framework.MainFramework;

public class Traverse extends MainFramework {

    @Override
    public String status() {
        return "Walking to mine";
    }

    @Override
    public boolean shouldProceed() {
        return !Inventory.isFull()
                && Vars.get().previousTile != null
                && !Player.getPosition().equals(Vars.get().previousTile)
                && Vars.get().currentRock == null;
    }

    @Override
    public void proceed() {
        if (!Vars.get().previousTile.isOnScreen() && Player.getPosition().distanceTo(Vars.get().previousTile) >= 15) {
            WalkingHandler.walkAndSleep(Vars.get().previousTile);
        }
        else {
            Walking.blindWalkTo(Vars.get().previousTile);
            Timing.waitCondition(() -> //wait until our player needs to move again, so we don't spam click near the destination
                            (!Player.isMoving()
                                    || Player.getPosition().equals(Vars.get().previousTile)),
                    General.randomSD(152, 6124, 3512, 400));
        }
    }
}
