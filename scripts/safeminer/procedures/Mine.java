package scripts.safeminer.procedures;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.*;
import org.tribot.api2007.types.RSObject;
import scripts.bbuu20_api.objhandlers.WalkingHandler;
import scripts.dax_api.api_lib.DaxWalker;
import scripts.safeminer.data.Vars;
import scripts.bbuu20_api.framework.MainFramework;
import scripts.bbuu20_api.util.Antiban;
import scripts.safeminer.util.Calculations;

public class Mine extends MainFramework {

    private long lastMiningWaitTime, averageMiningWaitTime, totalMiningWaitTime, totalMiningInstances;

    private void updateAntibanStatistics(long startedMining) {
        this.lastMiningWaitTime = System.currentTimeMillis() - startedMining;
        this.totalMiningInstances++;
        this.totalMiningWaitTime+=lastMiningWaitTime;
        this.averageMiningWaitTime = totalMiningWaitTime / totalMiningInstances;
        System.out.println(averageMiningWaitTime);
    }

    @Override
    public String status() {
        return "Mining";
    }

    @Override
    public boolean shouldProceed() {
        return !Inventory.isFull() &&
                Vars.get().currentRock != null;
    }

    @Override
    public void proceed() {
        RSObject nextRock = Calculations.findNextRock(); //find our next rock

        if (nextRock != null && Calculations.rockIsDepleted()) { //if the next rock exists, and our current rock is depleted, or doesn't exist
            if (nextRock.isOnScreen()) {
                long startedMining = System.currentTimeMillis(); //start our timer
                Vars.get().currentRock = nextRock; //the current rock is now what was the next rock
                Vars.get().currentRockTile = Vars.get().currentRock.getPosition(); //get the position of the rock we're mining

                Antiban.get().generateAndSleep((int)averageMiningWaitTime); //sleep for an abc generated amount of time
                DynamicClicking.clickRSObject(Vars.get().currentRock, ""); //click our rock
                Antiban.get().generateSupportingTrackerInfo((int)averageMiningWaitTime);

                General.sleep( //give our player enough time to start walking/mining. No need for abc sleep here
                        General.randomSD(732, 1823, 983, 400)
                                * Player.getPosition().distanceTo(Vars.get().currentRock));

                Timing.waitCondition(() -> !Player.isMoving(), General.random(5000, 10000)); //wait until our player is done moving toward the rock

                Vars.get().previousTile = Player.getPosition(); //update our mining position to the most ideal tile

                while (!Calculations.rockIsDepleted()) { //wait until the rock is depleted
                    Antiban.get().idleTimedActions(); //perform timed actions while mining
                }
                updateAntibanStatistics(startedMining); //update our average time spent mining
                Vars.get().currentRock = null; //after finishing mining, we must set currentRock = null
            }
            else {
                WalkingHandler.walkAndSleep(nextRock.getPosition());
            }
        }
    }
}
