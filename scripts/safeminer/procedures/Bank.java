package scripts.safeminer.procedures;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.*;
import scripts.bbuu20_api.objhandlers.WalkingHandler;
import scripts.safeminer.data.Finals;
import scripts.safeminer.data.Vars;
import scripts.bbuu20_api.framework.MainFramework;

public class Bank extends MainFramework {

    @Override
    public String status() {
        return "Banking";
    }

    @Override
    public boolean shouldProceed() {
        return Vars.get().shouldBank && Inventory.isFull();
    }

    @Override
    public void proceed() {
        switch (generateSubtasks()) {
            case WALKING_TO_BANK:
                WalkingHandler.walkToBankAndSleep();
                break;
            case OPENING_BANK:
                Banking.openBank();
                Timing.waitCondition(Banking::isBankScreenOpen, General.random(1000, 10000));
                break;
            case DEPOSITING_ITEMS:
                Banking.depositAllExcept(Finals.PICK_IDS);
                break;
        }
    }

    private enum subtasks {
        WALKING_TO_BANK,
        OPENING_BANK,
        DEPOSITING_ITEMS
    }

    private subtasks generateSubtasks() {
        if (!Banking.isInBank())
            return subtasks.WALKING_TO_BANK;
        if (!Banking.isBankScreenOpen())
            return subtasks.OPENING_BANK;
        return subtasks.DEPOSITING_ITEMS;
    }
}
