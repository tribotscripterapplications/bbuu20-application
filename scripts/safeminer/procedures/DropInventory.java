package scripts.safeminer.procedures;

import scripts.safeminer.data.Finals;
import scripts.safeminer.data.Vars;
import scripts.bbuu20_api.framework.MainFramework;
import org.tribot.api2007.Inventory;

public class DropInventory extends MainFramework {
    @Override
    public String status() {
        return "Dropping Inventory";
    }

    @Override
    public boolean shouldProceed() {
        return !Vars.get().shouldBank && Inventory.isFull();
    }

    @Override
    public void proceed() {
        Inventory.dropAllExcept(Finals.PICK_IDS);
    }
}
